CREATE TABLE `users` (
	`id` DECIMAL(19,2),
	`name` VARCHAR(255),
	`phone_no` VARCHAR(255),
	PRIMARY KEY (`id`)
);
