package com.jpop4.controller;

import com.jpop4.client.BookServiceClient;
import com.jpop4.client.dto.BookDto;
import com.jpop4.client.dto.UserDto;
import com.jpop4.dto.LibraryDto;
import com.jpop4.service.LibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;

@RestController
//@RequestMapping(value = "/library", produces = MediaType.APPLICATION_JSON_VALUE)
public class LibraryController {

    @Autowired
    private LibraryService libraryService;

    @Autowired
    private BookServiceClient bookServiceClient;

    @GetMapping
    public String getLibrary(){ return "My Library";}

    @GetMapping("/books")
    @ResponseBody
    public List<BookDto> getAllBookDetails() {
        return bookServiceClient.findAllBookDetails();
    }

    @GetMapping("/books/{bookId}")
    public BookDto getBookDetailsById(@PathVariable BigInteger bookId) {
        return libraryService.findBookDetailsById(bookId);
    }

    @PostMapping("/books")
    public boolean addBookDetails(@RequestBody BookDto bookDto) {
        return libraryService.addBookDetails(bookDto);
    }

    @DeleteMapping("/books/{bookId}")
    public boolean removeBookDetails(@PathVariable BigInteger bookId) {
        return libraryService.removeBookDetails(bookId);
    }

    @GetMapping("/users")
    public List<UserDto> getAllUserDetails() {
        return libraryService.findAllUserDetails();
    }

    @GetMapping("/users/{userId}")
    public LibraryDto getAllIssuedBookDetailsByUserId(@PathVariable BigInteger userId) {
        return libraryService.getAllIssuedBookDetailsByUserId(userId);
    }

    @PostMapping("/users")
    public boolean addUserDetails(@RequestBody UserDto userDto) {
        return libraryService.addUserDetails(userDto);
    }

    @DeleteMapping("/users/{userId}")
    public boolean deleteUserDetailsById(@PathVariable BigInteger userId) {
        return libraryService.deleteUserDetailsById(userId);
    }

    @PutMapping("/users")
    public UserDto updateUserDetails(@RequestBody UserDto userDto) {
        return libraryService.updateUserDetails(userDto);
    }

    @PostMapping("/users/{userId}/books/{bookId}")
    public boolean issueTheBook(@PathVariable BigInteger userId,
                             @PathVariable BigInteger bookId) {
        return libraryService.issueTheBook(userId, bookId);
    }

    @DeleteMapping("/users/{userId}/books/{bookId}")
    public boolean releaseTheBook(@PathVariable BigInteger userId,
                                  @PathVariable BigInteger bookId) {
        return libraryService.releaseTheBook(userId, bookId);
    }

}
