package com.jpop4.service;

import com.jpop4.client.BookServiceClient;
import com.jpop4.client.UserServiceClient;
import com.jpop4.client.dto.BookDto;
import com.jpop4.client.dto.UserDto;
import com.jpop4.dto.LibraryDto;
import com.jpop4.repository.LibraryRepository;
import com.jpop4.repository.entity.Library;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

@Service
public class LibraryService {

    @Qualifier("com.jpop4.client.BookServiceClient")
    @Autowired
    private BookServiceClient bookClient;

    @Qualifier("com.jpop4.client.UserServiceClient")
    @Autowired
    private UserServiceClient userClient;

    @Autowired
    private LibraryRepository libraryRepository;

    public List<BookDto> findAllBookDetails() {
        return bookClient.findAllBookDetails();
    }

    public BookDto findBookDetailsById(BigInteger bookId) {
        return bookClient.findBookDetails(bookId);
    }

    public boolean addBookDetails(BookDto bookDto) {
        return bookClient.addBookDetails(bookDto);
    }

    public boolean removeBookDetails(BigInteger bookId) {
        libraryRepository.deleteById(bookId);
        return bookClient.removeBookDetails(bookId);
    }

    public List<UserDto> findAllUserDetails() {
        return userClient.findAllUserDetails();
    }

    public LibraryDto getAllIssuedBookDetailsByUserId(BigInteger userId) {
        UserDto userDetails = userClient.findUserDetails(userId);

        List<Library> issuedBooks = libraryRepository.findAllByUserId(userDetails.getId());
        LibraryDto dto = new LibraryDto();
        dto.setUserId(userDetails.getId());
        dto.setUserName(userDetails.getName());

        issuedBooks.forEach(o ->
                dto.getIssuedListOfBooks()
                        .add(bookClient.findBookDetails(o.getBookId()))
        );
        return dto;
    }

    public boolean addUserDetails(UserDto userDto) {
        return userClient.addUserDetails(userDto);
    }

    public boolean deleteUserDetailsById(BigInteger userId) {
        libraryRepository.deleteAllByUserId(userId);
        return userClient.removeUserDetails(userId);
    }

    public UserDto updateUserDetails(UserDto userDto) {
        return userClient.updateUserDetails(userDto);
    }

    public boolean issueTheBook(BigInteger userId, BigInteger bookId) {
        Library libraryEntity = new Library();
        libraryEntity.setUserId(userId);
        libraryEntity.setBookId(bookId);
        libraryRepository.save(libraryEntity);

        return true;
    }

    public boolean releaseTheBook(BigInteger userId, BigInteger bookId) {
        libraryRepository.deleteByUserIdAndBookId(userId, bookId);
        return true;
    }
}
