package com.jpop4.dto;

import com.jpop4.client.dto.BookDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LibraryDto {
    private BigInteger userId;
    private String userName;
    private List<BookDto> issuedListOfBooks = new ArrayList<>();
}
