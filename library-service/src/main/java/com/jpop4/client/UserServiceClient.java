package com.jpop4.client;

import com.jpop4.client.dto.UserDto;
import com.jpop4.client.fallback.UserServiceClientFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;

@FeignClient(name = "user-service", fallback = UserServiceClientFallback.class)
@Component
public interface UserServiceClient {
    @GetMapping("/users/{userId}")
    public UserDto findUserDetails(@PathVariable BigInteger userId);

    @GetMapping("/users")
    public List<UserDto> findAllUserDetails();

    @PostMapping("/users")
    public boolean addUserDetails(@RequestBody UserDto userDto);

    @DeleteMapping("/users/{userId}")
    public boolean removeUserDetails(@PathVariable BigInteger userId);

    @PutMapping("/users")
    public UserDto updateUserDetails(@RequestBody UserDto userDto);
}
