package com.jpop4.client.fallback;

import com.jpop4.client.UserServiceClient;
import com.jpop4.client.dto.UserDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.Collections;
import java.util.List;

@Slf4j
@Component
public class UserServiceClientFallback implements UserServiceClient {
    @Override
    public UserDto findUserDetails(BigInteger userId) {
        log.error("UserServiceClientFallback#findUserDetails called");
        return null;
    }

    @Override
    public List<UserDto> findAllUserDetails() {
        log.error("UserServiceClientFallback#findAllUserDetails called");
        return Collections.emptyList();
    }

    @Override
    public boolean addUserDetails(UserDto userDto) {
        log.error("UserServiceClientFallback#addUserDetails called");
        return false;
    }

    @Override
    public boolean removeUserDetails(BigInteger userId) {
        log.error("UserServiceClientFallback#removeUserDetails called");
        return false;
    }

    @Override
    public UserDto updateUserDetails(UserDto userDto) {
        log.error("UserServiceClientFallback#updateUserDetails called");
        return null;
    }
}
