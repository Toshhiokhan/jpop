package com.jpop4.client.fallback;

import com.jpop4.client.BookServiceClient;
import com.jpop4.client.dto.BookDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
@Component
public class BookServiceClientFallback implements BookServiceClient {
    @Override
    public BookDto findBookDetails(BigInteger bookId) {
        log.error("BookServiceClientFallback#findBookDetails called");
        return null;
    }

    @Override
    public List<BookDto> findAllBookDetails() {
        log.error("BookServiceClientFallback#findAllBookDetails called");
        List<BookDto> books = new ArrayList<>();
        BookDto bookDto = new BookDto();
        bookDto.setAuthor("Perwez");
        bookDto.setId(BigInteger.ONE);
        bookDto.setCategory("Thriller");
        books.add(bookDto);
        //return bookService.getAllBookDetails();
        return books;
       // return Collections.emptyList();
    }

    @Override
    public boolean addBookDetails(BookDto bookDto) {
        log.error("BookServiceClientFallback#addBookDetails called");
        return false;
    }

    @Override
    public boolean removeBookDetails(BigInteger userId) {
        log.error("BookServiceClientFallback#removeBookDetails called");
        return false;
    }
}
