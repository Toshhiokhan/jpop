package com.jpop4.client;

import com.jpop4.client.dto.BookDto;
import com.jpop4.client.fallback.BookServiceClientFallback;
import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;

@FeignClient(name = "book-service", url="http://localhost:9080",fallback=BookServiceClientFallback.class )
@Component
public interface BookServiceClient {

    String AUTH_TOKEN = "Authorization";

    @GetMapping("/books/{bookId}")
    public BookDto findBookDetails(@PathVariable BigInteger bookId);

    @GetMapping("/books")
    //@Headers("Content-Type: application/json")
    public List<BookDto> findAllBookDetails();

    @PostMapping("/books")
    public boolean addBookDetails(@RequestBody BookDto bookDto);

    @DeleteMapping("/books/{bookId}")
    public boolean removeBookDetails(@PathVariable BigInteger userId);
}
