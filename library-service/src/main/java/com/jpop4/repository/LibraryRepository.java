package com.jpop4.repository;

import com.jpop4.repository.entity.Library;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface LibraryRepository extends JpaRepository<Library, BigInteger> {
    List<Library> findAllByUserId(BigInteger userId);

    void deleteAllByUserId(BigInteger userId);

    void deleteByUserIdAndBookId(BigInteger userId, BigInteger bookId);
}
