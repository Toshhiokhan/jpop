package com.jpop4.controller.advice;

import com.jpop4.exceptions.BookNotFoundException;
import com.jpop4.utils.handler.dto.ErrorDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
public class GlobalBEControllerExceptionHandler {

    @ExceptionHandler(value = BookNotFoundException.class)
    public ResponseEntity<ErrorDto> handleBookNotFoundException(BookNotFoundException exception) {
        log.error(exception.toString());
        //return new ResponseEntity<>(exception.getMessage(), HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(
                ErrorDto
                        .builder()
                        .exception(exception.getClass().getSimpleName())
                        .message(exception.getMessage())
                        .build(),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = RuntimeException.class)
    public ResponseEntity<String> handleRuntimeException(RuntimeException exception) {
        log.error(exception.toString());
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
