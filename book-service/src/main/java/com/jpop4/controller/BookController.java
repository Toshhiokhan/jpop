package com.jpop4.controller;

import com.jpop4.domain.BookDto;
import com.jpop4.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/books", produces = MediaType.APPLICATION_JSON_VALUE)
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping("/{bookId}")
    public BookDto findBookDetails(@PathVariable BigInteger bookId) {
        return bookService.getBookDetails(bookId);
    }

    @GetMapping
    public List<BookDto> findAllBookDetails() {
        List<BookDto> books = new ArrayList<>();
        BookDto bookDto = new BookDto();
        bookDto.setAuthor("Perwez");
        bookDto.setId(BigInteger.ONE);
        bookDto.setDescription("Feign Client ");
        books.add(bookDto);
        //return bookService.getAllBookDetails();
        return books;
    }

    @PostMapping
    public boolean addBookDetails(@RequestBody BookDto bookDto) {
        return bookService.addBookDetails(bookDto);
    }

    @DeleteMapping("/{bookId}")
    public boolean removeBookDetails(@PathVariable BigInteger bookId) {
        return bookService.deleteBookDetails(bookId);
    }

    @PutMapping
    public BookDto updateBookDetails(@RequestBody BookDto bookDto) {
        return bookService.updateBookDetails(bookDto);
    }

    @GetMapping("/v2/{bookId}")
    public BookDto findBookDetailsUriVersioning(@PathVariable BigInteger bookId) {
        return bookService.getBookDetails(bookId);
    }

    @GetMapping(value = "/{bookId}", params = "version=2")
    public BookDto findBookDetailsQueryParamVersioning(@PathVariable BigInteger bookId) {
        return bookService.getBookDetails(bookId);
    }

    @GetMapping(value = "/{bookId}", produces = "application/jpop-v2+json")
    public BookDto findBookDetailsContentNegotiationVersioning(@PathVariable BigInteger bookId) {
        return bookService.getBookDetails(bookId);
    }
}
