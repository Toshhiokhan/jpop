package com.jpop4.utils.handler;

import com.jpop4.utils.handler.dto.ErrorDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseHandler {
    private ResponseHandler() {}

    public static <T> ResponseEntity<T> getInternalServerErrorResponseEntity() {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public static ResponseEntity<ErrorDto> getInternalServerErrorResponseEntity(Exception ex) {
        return new ResponseEntity<ErrorDto>(getErrorDtoFromException(ex), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public static <T> ResponseEntity<T> getOkResponseEntity(T returnObject) {
        return new ResponseEntity<>(returnObject, HttpStatus.OK);
    }

    public static <T> ResponseEntity<T> getCreatedResponseEntity(T returnObject) {
        return new ResponseEntity<>(returnObject, HttpStatus.CREATED);
    }

    public static <T> ResponseEntity<T> getNotFoundResponseEntity() {
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    public static <T> ResponseEntity<T> getNoContentResponseEntity() {
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    private static ErrorDto getErrorDtoFromException(Exception ex) {
        return ErrorDto.builder()
                .exception(ex.getClass().getSimpleName())
                .message(ex.getMessage())
                .build();
    }
}
