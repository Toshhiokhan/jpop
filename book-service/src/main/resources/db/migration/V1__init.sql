CREATE TABLE `books` (
	`id` DECIMAL(19,2),
	`name` VARCHAR(255),
	`author` VARCHAR(255),
	`description` VARCHAR(255),
	`category` VARCHAR(255),
	PRIMARY KEY (`id`)
);
